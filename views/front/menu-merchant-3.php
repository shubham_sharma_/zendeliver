<div class="box_style_2" id="main_menu">
<?php if(is_array($menu) && count($menu)>=1):?>
<h2 class="inner">Menu</h2>

<?php foreach ($menu as $val):?>
<h3 class=" cat-<?php echo $val['category_id']?>" id="starters"><?php echo qTranslate($val['category_name'],'category_name',$val)?></h3>
<!--p>
	Te ferri iisque aliquando pro, posse nonumes efficiantur in cum. Sensibus reprimique eu pro. Fuisset mentitum deleniti sit ea.
</p-->
<table class="table table-striped cart-list">
<thead>
<tr><th>Item</th><th>Price</th><th>Order</th></tr>
</thead>
<tbody>
<?php if (!empty($val['item'])):?>
<?php 
$k =1;
foreach ($val['item'] as $val_item):?>

<?php 
$atts='';
if ( $val_item['single_item']==2){
	  $atts.='data-price="'.$val_item['single_details']['price'].'"';
	  $atts.=" ";
	  $atts.='data-size="'.$val_item['single_details']['size'].'"';
}
?> 
<tr> 
	<td>
		<h5><?php echo $k++.'. '. qTranslate($val_item['item_name'],'item_name',$val_item)?></h5>
		<p class="small food-description read-more">
			<?php echo qTranslate($val_item['item_description'],'item_description',$val_item)?>
		</p>
	</td>
	<td>
		<strong><?php echo FunctionsV3::getItemFirstPrice($val_item['prices'],$val_item['discount'])?></strong>
	</td>
	<td class="options">
<a href="javascript:;" class="dsktop menu-item <?php echo $val_item['not_available']==2?"item_not_available":''?>"
rel="<?php echo $val_item['item_id']?>"
data-single="<?php echo $val_item['single_item']?>" 
<?php echo $atts;?>
><i class="icon_plus_alt2"></i></a>
	</td>
</tr>
<?php endforeach;?>
<?php else :?>
<p class="small text-danger"><?php echo t("no item found on this category")?></p>
<?php endif;?>
</tbody>
</table>
<?php endforeach;?>


</div> <!--box-->
<?php else :?>
<p class="text-danger"><?php echo t("This restaurant has not published their menu yet.")?></p>
<?php endif;?>


<style>
.small.food-description.read-more > span {
  font-size: 13px !important;
}
</style>

