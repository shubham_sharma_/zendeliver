<?php
$merchant_id=$val['merchant_id'];
$ratings=Yii::app()->functions->getRatings($merchant_id);   
$merchant_delivery_distance=getOption($merchant_id,'merchant_delivery_miles');
$distance_type='';
$total = $ratings['ratings'];
$rate = explode(",",$_SESSION['filter_rating']);
// $cs = '';
// if(!empty($_SESSION['filter_rating']) || $_SESSION['filter_rating'] != 0){	
	// if(!in_array($total,$rate)){
		// $cs = 'display:none;';
	// }
// }

/*fallback*/
if ( empty($val['lat'])){
	if ($lat_res=Yii::app()->functions->geodecodeAddress($val['merchant_address'])){        
		$val['latitude']=$lat_res['lat'];
		$val['lontitude']=$lat_res['long'];
	} 
}
?>

<div style="<?php echo $cs; ?>" class="infinite-item strip_list wow fadeIn" data-wow-delay="0.1s">
	<?php if ( $val['is_sponsored']==2):?>
	<div class="ribbon_1">
		Sponsored
	</div>
	<?php endif;?>
	<?php if ($offer=FunctionsV3::getOffersByMerchant($merchant_id)):?>
       <div class="ribbon-offer"><span><?php echo $offer;?></span></div>
    <?php endif;?>
	
	<div class="row">
		<div class="col-md-9 col-sm-9">
			<div class="desc">
				<div class="thumb_strip">
					<a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>">
						<img src="<?php echo FunctionsV3::getMerchantLogo($merchant_id);?>">
					</a>
				</div>
				<div class="rating">
					 <?php
						
						for($s=1; $s <= 5; $s++) 
						{
							if($s <= $total){
								echo '<i class="icon_star voted"></i>';
							}else{
								echo '<i class="icon_star"></i>';
							}
						}
						?> (<small><a href="#0"><?php echo $ratings['votes']; ?> reviews</a></small>)
				</div>
				<h3><?php echo $val['restaurant_name']?></h3>
				<div class="type">
					<?php echo FunctionsV3::displayCuisine($val['cuisine']);?>
				</div>
				<div class="location">
					<?php echo $val['merchant_address']?> <!--span class="opening">Opens at 17:00.</span--> <br />
					<?php 
						if (!empty(FunctionsV3::prettyPrice($val['minimum_order']))){ echo t("Minimum Order").": ".FunctionsV3::prettyPrice($val['minimum_order']); }?><br />
					<?php echo t("Delivery Est")?>: <?php echo FunctionsV3::getDeliveryEstimation($merchant_id)?><br />
					<?php 
						if (!empty($merchant_delivery_distance)){
							echo t("Delivery Distance").": ".$merchant_delivery_distance." $distance_type";
						} else echo  t("Delivery Distance").": ".t("not available");
					?>
				</div>
				<?php echo FunctionsV3::displayServicesList($val['service'])?>
			</div>
		</div>
		<div class="col-md-3 col-sm-3">
			<div class="go_to">
				<div>
					<a href="<?php echo Yii::app()->createUrl('store/menu/merchant/'.$val['restaurant_slug'])?>" class="btn_1">View Menu</a>
				</div>
			</div>
		</div>
	</div><!-- End row-->
</div><!-- End strip_list-->