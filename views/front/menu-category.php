
<?php if(is_array($menu) && count($menu)>=1):?>
<div class="category1" id="cat_nav">
<?php foreach ($menu as $val):?>
 <a href="javascript:;" class="category-child relative goto-category" data-id="cat-<?php echo $val['category_id']?>" >
  <?php echo qTranslate($val['category_name'],'category_name',$val)?>
  <span>(<?php echo is_array($val['item'])?count($val['item']):'0';?>)</span>

 </a>
<?php endforeach;?>
</div>
<?php endif;?>

<style>
.sspading {
  padding-left: 8px;
  padding-right: 0;
}

.sspading a.btn_side{
	margin-bottom:10px;
}
#cat_nav {
    list-style: outside none none;
    margin: 0;
    padding: 0;
}
#cat_nav a {
    border-bottom: 1px solid #ededed;
}
#cat_nav a {
    color: #555;
    position: relative;
}
#cat_nav a span {
    color: #999;
    font-size: 11px;
}
#cat_nav a::after {
    content: "";
    font-family: "fontello";
    position: absolute;
    right: 15px;
    top: 15px;
}
#cat_nav a:last-child {
    border-bottom: 0 none;
    
}
#cat_nav a:first-child a:hover, #cat_nav a:first-child a.active {
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
}
#cat_nav a:last-child a:hover, #cat_nav a:last-child a.active {
    border-bottom-left-radius: 3px;
    border-bottom-right-radius: 3px;
}
#cat_nav a {
    display: block;
    padding: 15px 10px;
}
#cat_nav a:hover, #cat_nav a.active {
    background: #f9f9f9 none repeat scroll 0 0;
    color: #111;
}

</style>