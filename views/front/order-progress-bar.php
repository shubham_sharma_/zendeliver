<?php
$kr_merchant_slug=isset($_SESSION['kr_merchant_slug'])?$_SESSION['kr_merchant_slug']:'';

if (isset($_SESSION['search_type'])){
	switch ($_SESSION['search_type']) {
		case "kr_search_foodname":			
			$search_key='foodname';
			$search_str= isset($_SESSION['kr_search_foodname'])?$_SESSION['kr_search_foodname']:'';
			break;
			
		case "kr_search_category":			
			$search_key='category';
			$search_str=isset($_SESSION['kr_search_category'])?$_SESSION['kr_search_category']:'';
			break;
			
		case "kr_search_restaurantname":
			$search_str=isset($_SESSION['kr_search_restaurantname'])?$_SESSION['kr_search_restaurantname']:'';
			$search_key='restaurant-name';
			break;	
		
		case "kr_search_streetname":
			$search_str=isset($_SESSION['kr_search_streetname'])?$_SESSION['kr_search_streetname']:'';
			$search_key='street-name';
			break;	

		case "kr_postcode":	
		    $search_str=isset($_SESSION['kr_postcode'])?$_SESSION['kr_postcode']:'';
		    $search_key='zipcode';
			break;	
			
		default:
			$search_str=isset($_SESSION['kr_search_address'])?$_SESSION['kr_search_address']:'';
			$search_key='s';
			break;
	}
}
if($status == 'step')
{
	$s1 = 'disabled';
	$s2 = 'disabled';
	$s3 = 'disabled';
	if($step==1){
		$s1 = 'active';
	}
	if($step==2){
		$s1 = 'complete';
		$s2 = 'active';
	}
	if($step==3){
		$s1 = 'complete';
		$s2 = 'complete';
		$s3 = 'complete';
	}
	
	
	
?>
<!-- SubHeader =============================================== -->
<section class="parallax-window" id="short" data-parallax="scroll" data-image-src="<?php echo assetsURL()."/images/banner-5.jpg"?>" data-natural-width="1400" data-natural-height="350">
    <div id="subheader">
    	<div id="sub_content">
    	 <h1><?php echo isset($h1)?$h1:''?></h1>
		 <?php if($sub_text){?><p><?php echo $sub_text; ?></p><?php } ?>
            <div class="bs-wizard">
                <div class="col-xs-4 bs-wizard-step <?php echo $s1; ?>">
                  <div class="text-center bs-wizard-stepnum"><strong>1.</strong> Your details</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="#0" class="bs-wizard-dot"></a>
                </div>
                               
                <div class="col-xs-4 bs-wizard-step <?php echo $s2; ?>">
                  <div class="text-center bs-wizard-stepnum"><strong>2.</strong> Payment</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="cart_2.html" class="bs-wizard-dot"></a>
                </div>
            
              <div class="col-xs-4 bs-wizard-step <?php echo $s3; ?>">
                  <div class="text-center bs-wizard-stepnum"><strong>3.</strong> Finish!</div>
                  <div class="progress"><div class="progress-bar"></div></div>
                  <a href="cart_3.html" class="bs-wizard-dot"></a>
                </div>  
		</div><!-- End bs-wizard --> 
        </div><!-- End sub_content -->
	</div><!-- End subheader -->
</section><!-- End section -->
<!-- End SubHeader ============================================ -->
<?php 
}
$show_bar = false;
if ($show_bar):?>
<div class="order-progress-bar">
  <div class="container">
      <div class="row">
      
        <div class="col-md-3 col-xs-3 ">
          <a class="active" href="<?php echo Yii::app()->createUrl('/store')?>"><?php echo t("Search")?></a>  
        </div>
        
        <div class="col-md-3 col-xs-3 ">
           <a class="<?php echo $step>=2?"active":"inactive"; echo $step==2?" current":"";?>" 
           href="<?php echo Yii::app()->createUrl('store/searcharea',array($search_key=>$search_str))?>">
           <?php echo t("Pick Merchant")?></a>
        </div>
        
        <div class="col-md-3 col-xs-3">
        <a class="<?php echo $step>=3?"active":"inactive"; echo $step==3?" current":"";?> "
         href="<?php echo Yii::app()->createUrl('/store/menu/merchant/'.$kr_merchant_slug)?>">
        <?php echo t("Create your order")?></a>
        </div>
        
        <div class="col-md-3 col-xs-3 ">
        <a class="<?php echo $step>=4?"active":"inactive"; echo $step==4?" current":"";?> "
         href="javascript:;"><?php echo t("Checkout")?></a>
        </div>
      
        
      </div><!-- row-->
  </div> <!--container-->
  
   <div class="border progress-dot mytable">    
     <a href="<?php echo Yii::app()->createUrl('/store')?>" class="mycol selected" ><i class="ion-record"></i></a>
     <a href="javascript:;" class="mycol 
     <?php echo $step>=2?"selected":'';?>" ><i class="ion-record"></i></a>
     
     <a href="javascript:;" class="mycol <?php echo $step>=3?"selected":'';?>" ><i class="ion-record"></i></a>
     
     <a href="javascript:;" class="mycol <?php echo $step>=4?"selected":'';?>"><i class="ion-record"></i></a>
     
  </div> <!--end progress-dot-->
  
</div> <!--order-progress-bar-->
<?php endif;?>