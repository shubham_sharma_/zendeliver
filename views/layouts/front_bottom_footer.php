<?php

if (!Yii::app()->functions->isClientLogin()){
?>
<!-- Login modal -->   
<div class="modal fade" id="login_2" tabindex="-1" role="dialog" aria-labelledby="myLogin" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content modal-popup"> 
				<a href="#" class="close-link"><i class="icon_close_alt2"></i></a>
				<form action="" method="post" class="popup-form forms has-validation-callback lform" id="forms-login">
				<input id="action" type="hidden" name="action" value="clientLogin">
				<input id="currentController" type="hidden" name="currentController" value="store">
                	<div class="login_icon"><i class="icon_lock_alt"></i></div>
					<input id="username1" type="text" required="1" class="form-control form-white" name="username" placeholder="Username">
					<input id="password1" type="password" required="1" class="form-control form-white" name="password" placeholder="Password">
					<div class="text-left">
						<a onclick="changeForm(2)" href="javascript:;" >Forgot Password?</a>
					</div>
					<button type="submit" class="btn btn-submit">Login</button>  
				</form>
				
				<form action="" onsubmit="return false;" method="post" class="popup-form ffrom frm-modal-forgotpass has-validation-callback" id="frm-modal-forgotpass" style="display:none;">
					<div class="login_icon"><i class="icon_lock_alt"></i></div>
					<input id="action" type="hidden" name="action" value="forgotPassword">
					<input id="do-action" type="hidden" name="do-action" value="">
					<input id="username-email" type="text" required="1" class="form-control form-white" name="username-email" placeholder="Email address">
					<div class="text-left">
							<a onclick="changeForm(1)" href="javascript:;" >Click for Login</a>
						</div>
					<button type="submit" class="btn btn-submit">Retrieve Password</button>
				</form>
			</div>
		</div>
	</div><!-- End modal -->   
	 
	
	<script>
	function changeForm(mode){
		// alert(mode);
		if(mode == 1)
		{
			jQuery(".lform").css("display","block");
			jQuery("#frm-modal-forgotpass").css("display","none");
		
		}else
		{
			jQuery(".lform").css("display","none");
			jQuery("#frm-modal-forgotpass").css("display","block");
		}
	}
	</script>
	

	
	
	
    
<!-- Register modal -->   
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="myRegister" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content modal-popup">
			<a href="#" class="close-link"><i class="icon_close_alt2"></i></a>
			<form action="#" class="popup-form" id="myRegister">
				<div class="login_icon"><i class="icon_lock_alt"></i></div>
				<input type="text" class="form-control form-white" placeholder="Name">
				<input type="text" class="form-control form-white" placeholder="Last Name">
				<input type="email" class="form-control form-white" placeholder="Email">
				<input type="text" class="form-control form-white" placeholder="Password"  id="password1">
				<input type="text" class="form-control form-white" placeholder="Confirm password"  id="password2">
				<div id="pass-info" class="clearfix"></div>
				<div class="checkbox-holder text-left">
					<div class="checkbox">
						<input type="checkbox" value="accept_2" id="check_2" name="check_2" />
						<label for="check_2"><span>I Agree to the <strong>Terms &amp; Conditions</strong></span></label>
					</div>
				</div>
				<button type="submit" class="btn btn-submit">Register</button>
			</form>
		</div>
	</div>
</div><!-- End Register modal --><!--[if lte IE 8]>
	<p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>.</p>
<![endif]-->
   <?php
}
?>   
    <!-- Footer ================================================== -->
    <footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-3">
			<h3>Secure payments by yoPay®</h3>
			<p>
			<img class="img-responsive" alt="" src="<?php echo assetsURL()?>/img/cards.png">
			</p>
			
                <p>
                    <?php FunctionsV3::getFooterAddress();?>
                </p>
            </div>
            <div class="col-md-3 col-sm-3">
				<?php if ($theme_hide_footer_section1!=2):?>
				   <h3><?php 
				   
				   
				   
				   
				   echo t("Menu")?></h3>
				  
				   <?php if (is_array($menu) && count($menu)>=1):?>
				   <?php foreach ($menu as $val): ?>
				   <li>
					 <a 
					   href="<?php echo FunctionsV3::customPageUrlNew($val)?>" <?php FunctionsV3::openAsNewTab($val)?> >
					  <?php echo $val['page_name']?></a>
				   </li>
				   <?php endforeach;?>
				   <li><a href="<?php echo Yii::app()->createUrl('/store/contact')?>" >Contacts</a></li>
				   <li><a href="<?php echo Yii::app()->createUrl('/store/signup')?>" >Login</a></li>
				   <li><a href="<?php echo Yii::app()->createUrl('/store/signup')?>" >Register</a></li>
				   <?php endif;?>
				   
				   <?php endif;?>
				
				
				<?php if ($theme_hide_footer_section2!=2):?>
				 <h3><?php echo t("Others")?></h3>
				 
				   <?php if (is_array($others_menu) && count($others_menu)>=1):?>
				   <?php foreach ($others_menu as $val):?>
				   <li>
					 <a 
					   href="<?php echo FunctionsV3::customPageUrl($val)?>" <?php FunctionsV3::openAsNewTab($val)?> >
					  <?php echo $val['page_name']?></a>
				   </li>
				   <?php endforeach;?>
				   <?php endif;?>
				 
				 <?php endif;?> 
            </div>
            <div class="col-md-3 col-sm-3" id="newsletter">
				<?php if ( getOptionA('disabled_subscription') == ""):?>
                <h3>Newsletter</h3>
                <p><?php echo t("Subscribe to our newsletter") ?></p>
                <div id="message-newsletter_2">
                </div>
				<form method="POST" id="frm-subscribe" class="frm-subscribe" onsubmit="return false;">
				<?php echo CHtml::hiddenField('action','subscribeNewsletter')?>
					<div class="form-group">
						<?php echo CHtml::textField('subscriber_email','',array(
								 'placeholder'=>t("E-mail"),
								 'required'=>true,
								 'class'=>"email form-control"
							   ))?>
					</div>
					<button class="btn_1"><?php echo t("Subscribe")?> </button>
				</form>
				<?php endif;?>				
            </div>
            <div class="col-md-2 col-sm-3">
                 <?php 
					if ($show_language<>1){
						echo '<h3>Settings</h3>';
						if ( $theme_lang_pos=="bottom" || $theme_lang_pos==""){
							echo CHtml::dropDownList('language-options','',
							 (array)FunctionsV3::getLanguage()
							 ,array(
							 'class'=>"language-options selectpicker",
							 'title'=>t("Select language")
							));
						}
					}
				?>
            </div>
        </div><!-- End row -->
		
		 <?php if ($social_flag<>1):?>
        <div class="row">
            <div class="col-md-12">
                <div id="social_footer">
                    <ul>
                        <?php if (!empty($fb_page)):?>
							<li><a href="<?php echo FunctionsV3::prettyUrl($fb_page)?>"><i class="icon-facebook"></i></a></li>
						<?php endif;?>
						<?php if (!empty($twitter_page)):?>
							<li><a href="<?php echo FunctionsV3::prettyUrl($twitter_page)?>"><i class="icon-twitter"></i></a></li>
						<?php endif;?>
						<?php if (!empty($google_page)):?>
							<li><a href="<?php echo FunctionsV3::prettyUrl($google_page)?>"><i class="icon-google"></i></a></li>
						<?php endif;?>
						<?php if (!empty($intagram_page)):?>
							<li><a href="<?php echo FunctionsV3::prettyUrl($intagram_page)?>"><i class="icon-instagram"></i></a></li>
						<?php endif;?>
						<?php if (!empty($youtube_url)):?>
							<li><a href="<?php echo FunctionsV3::prettyUrl($youtube_url)?>"><i class="icon-youtube-play"></i></a></li>
						<?php endif;?>
                    </ul>
                    <p>
                        © Zen Deliver <?php echo date("Y"); ?>
                    </p>
                </div>
            </div>
        </div><!-- End row -->
		<?php endif;?>
    </div><!-- End container -->
    </footer>
    <!-- End Footer =============================================== -->
	
	
	
	
	
	