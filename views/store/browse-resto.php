<?php
$this->renderPartial('/front/default-header',array(
   'h1'=>t("Browse All Merchants"),
   'sub_text'=>t("choose from your favorite merchants below")
));
function clean($string) {
   //$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

   return preg_replace('/[^a-zA-Z0-9\s]/', '', $string); // Removes special chars.
}

$db_ext=new DbExt;    	 
$db_ext->qry("SET SQL_BIG_SELECTS=1");

$getAllCuisine = yii::app()->functions->getAllCuisine(); 
$newArr = array();
foreach($getAllCuisine as $k){		
	$cid =$k['cuisine_id']; 
	$and =" AND (cuisine LIKE '%\"$cid\"%') ";
	  $stmt = "SELECT *,
    	concat(street,' ',city,' ',state,' ',post_code) as merchant_address FROM {{view_merchant}} WHERE is_ready ='2'  $and ";    
 
	$ress = $db_ext->rst($stmt);

	foreach($ress as $res){
		if ( empty($res['lat'])){
			if ($lat_res=Yii::app()->functions->geodecodeAddress($res['merchant_address'])){        
				$res['latitude']=$lat_res['lat'];
				$res['lontitude']=$lat_res['long'];
			} 
		}  
		$mArr = array();
		$mArr['name'] = clean($res['restaurant_name']);
		$mArr['location_latitude'] = $res['latitude'];
		$mArr['location_longitude'] = $res['lontitude'];
		$mArr['map_image_url'] = FunctionsV3::getMerchantLogo($res['merchant_id']);
		$mArr['name_point'] = clean($res['restaurant_name']);
		$mArr['type_point'] = str_replace("'","",FunctionsV3::displayCuisine($res['cuisine']));
		$mArr['description_point'] = str_replace("'","",$res['merchant_address']);
		$mArr['url_point'] = Yii::app()->createUrl('store/menu/merchant/'.$res['restaurant_slug']);
		$newArr[$cid][] = $mArr;

	}
	
}

?>
<script>
	var Arrssss  = '<?php echo json_encode($newArr); ?>';
</script>


<div id="position">
	<div class="container">
		<ul>
			<li><a href="#0">Home</a></li>
			<li>Category</li>
		</ul>
	</div>
</div><!-- Position -->
<?php
 // exit;
?>
<div class="collapse" id="collapseMap">
	<div id="map" class="map"></div>
</div><!-- End Map -->



<!-- Content ================================================== -->
<div class="container margin_60_35">
	<div class="row">
    
		<div class="col-md-3">
			<p>
				<a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap">View on map</a>
			</p>
			<form action="" id="formFil" method="get">
			<div id="filters_col">
				<a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt">Filters <i class="icon-plus-1 pull-right"></i></a>
				<div class="collapse" id="collapseFilters">
					<div class="filter_type">
          
						<h6>Category</h6>
						<ul>
						<?php 
						
						foreach($getAllCuisine as $k){		
						$ckh ='';
						if(!empty($_GET['filter_cuisine'])){
							if(in_array($k['cuisine_id'],explode(",",$_GET['filter_cuisine']))){
								$ckh ='checked=""';
							}	
						}
					
						
						?>
						<li><label><input type="checkbox" <?php echo $ckh; ?> name="filcuisine[]" value="<?php echo $k['cuisine_id']; ?>" class="icheck filcuisine"> &nbsp; <?php echo ucfirst($k['cuisine_name']); ?> <!--small>(49)</small--></label></li>
						<?php
						}						
						?>
						</ul>
					</div>
					<?php
					$ckh5 ='';
					if(in_array(5,explode(",",$_GET['filter_rating']))){
						$ckh5 ='checked=""';
					}
					$ckh4 ='';
					if(in_array(4,explode(",",$_GET['filter_rating']))){
						$ckh4 ='checked=""';
					}
					$ckh3 ='';
					if(in_array(3,explode(",",$_GET['filter_rating']))){
						$ckh3 ='checked=""';
					}
					$ckh2 ='';
					if(in_array(2,explode(",",$_GET['filter_rating']))){
						$ckh2 ='checked=""';
					}
					$ckh1 ='';
					if(in_array(1,explode(",",$_GET['filter_rating']))){
						$ckh1 ='checked=""';
					}
					
					?>
					
					
					<div class="filter_type">
						<h6>Rating</h6>
						<ul>
							<li><label><input type="checkbox" <?php echo $ckh5; ?> name="filrating[]" class="icheck filrating" value="5"> &nbsp; <span class="rating">
							<i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i>
							</span></label></li>
							<li><label><input type="checkbox" <?php echo $ckh4; ?> name="filrating[]" class="icheck filrating" value="4"> &nbsp; <span class="rating">
							<i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i>
							</span></label></li>
							<li><label><input type="checkbox" <?php echo $ckh3; ?> name="filrating[]" class="icheck filrating" value="3"> &nbsp; <span class="rating">
							<i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i>
							</span></label></li>
							<li><label><input type="checkbox" <?php echo $ckh2; ?> name="filrating[]" class="icheck filrating" value="2"> &nbsp; <span class="rating">
							<i class="icon_star voted"></i><i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i>	
							</span></label></li>
							<li><label><input type="checkbox" <?php echo $ckh1; ?> name="filrating[]" class="icheck filrating" value="1"> &nbsp; <span class="rating">
							<i class="icon_star voted"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i><i class="icon_star"></i>
							</span></label></li>
						</ul>
					</div>
					<!--div class="filter_type">
						<h6>Options</h6>
						<ul class="nomargin">
							<li><label><input type="checkbox" class="icheck"> &nbsp; Delivery</label></li>
							<li><label><input type="checkbox" class="icheck"> &nbsp; Take Away</label></li>
							<li><label><input type="checkbox" class="icheck"> &nbsp; Distance 10Km</label></li>
							<li><label><input type="checkbox" class="icheck"> &nbsp; Distance 5Km</label></li>
						</ul>
					</div-->
				</div><!--End collapse -->
			</div><!--End filters col--> 
			</form>
		</div><!--End col-md -->

		<div class="col-md-9">
        
        <div id="tools">
				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="styled-select">
							<select name="sort_rating" id="sort_rating" onchange="window.location.href=this.value">
								<option value="" selected>Sort by</option>
								<option value="<?php echo Yii::app()->createUrl('/store/browse')?>">Restaurant List</option>
								<option value="<?php echo Yii::app()->createUrl('/store/browse/?tab=2')?>">Newest</option>
								<option value="<?php echo Yii::app()->createUrl('/store/browse/?tab=3')?>">Featured</option>
							</select>
						</div>
					</div>
					<div class="col-md-9 col-sm-6 hidden-xs">
						<a href="#" class="bt_filters"><i class="icon-th"></i></a>
					</div>
				</div>
			</div><!--End tools -->
			<?php
            if ( $tabs==1):
	            if (is_array($list['list']) && count($list['list'])>=1){
		            $this->renderPartial('/front/browse-list',array(
					   'list'=>$list,
					   'tabs'=>$tabs
					));
	            } else echo '<p class="text-danger">'.t("No restaurant found").'</p>';
            endif;
            ?>
			<?php          
            if ( $tabs==2):
	            if (is_array($list['list']) && count($list['list'])>=1){
		            $this->renderPartial('/front/browse-list',array(
					   'list'=>$list,
					   'tabs'=>$tabs			   
					));
	            } else echo '<p class="text-danger">'.t("No restaurant found").'</p>';
            endif;
            ?>
            <?php          
            if ( $tabs==3):
	            if (is_array($list['list']) && count($list['list'])>=1){
		            $this->renderPartial('/front/browse-list',array(
					   'list'=>$list,
					   'tabs'=>$tabs
					));
	            } else echo '<p class="text-danger">'.t("No restaurant found").'</p>';
            endif;
            ?>
			
					
            
		</div><!-- End col-md-9-->
        
	</div><!-- End row -->
</div><!-- End container -->
<!-- End Content =============================================== -->






























