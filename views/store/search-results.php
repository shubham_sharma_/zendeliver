<?php 
$search_address=isset($_GET['s'])?$_GET['s']:'';
if (isset($_GET['st'])){
	$search_address=$_GET['st'];
}
$this->renderPartial('/front/search-header',array(
   'search_address'=>$search_address,
   'total'=>$data['total']
));?>

<?php 
$this->renderPartial('/front/order-progress-bar',array(
   'step'=>2,
   'show_bar'=>true
));

echo CHtml::hiddenField('clien_lat',$data['client']['lat']);
echo CHtml::hiddenField('clien_long',$data['client']['long']);
?>







<!-- Content ================================================== -->
<div class="container margin_60_35">
	<div class="row">
    
		<div class="col-md-3">

			<div id="filters_col">
				<a data-toggle="collapse" href="#collapseFilters" aria-expanded="false" aria-controls="collapseFilters" id="filters_col_bt">Filters <i class="icon-plus-1 pull-right"></i></a>
				<div class="collapse" id="collapseFilters">
					
					
					
					<!--FILTER DELIVERY FEE-->           
           <div class="filter_type">
	           <h6>
	             <?php echo t("Delivery Fee")?>
	             </h6>
	            <ul>
	              <li>
	              <?php 
		          echo CHtml::checkBox('filter_by[]',false,array(
		          'value'=>'free-delivery',
		          'class'=>"filter_promo icheck"
		          ));
		          ?>
	              <?php echo t("Free Delivery")?>
	              </li>
	           </ul>
           </div> <!--filter-box-->
           <!--END FILTER DELIVERY FEE-->
					
					 <!--FILTER DELIVERY -->
				   <?php if ( $services=Yii::app()->functions->Services() ):?>
				   <div class="filter_type">
					   <h6>
						 <?php echo t("By Delivery")?>
						 </h6>
					   <ul>
						 <?php foreach ($services as $key=> $val):?>
						  <li>	           	              
						  <?php 
						   echo CHtml::radioButton('filter_delivery_type',
						   $filter_delivery_type==$key?true:false
						   ,array(
						  'value'=>$key,
						  'class'=>"filter_by filter_delivery_type icheck"
						  ));
						  ?>
						  <?php echo $val;?>   
						  </li>
						 <?php endforeach;?> 
					   </ul>
				   </div> <!--filter-box-->
				   <?php endif;?>
				   <!--END FILTER DELIVERY -->
					
					
					
					
					
					
					
					
					
					<!--FILTER CUISINE-->

				   <?php if ( $cuisine=Yii::app()->functions->Cuisine(false)):?>
				   <div class="filter_type">
						<h6>Category</h6>
						<ul>
						 <?php foreach ($cuisine as $val): ?>
						  <li><label>
						   <?php 
						  echo CHtml::checkBox('filter_cuisine[]',
						  in_array($val['cuisine_id'],(array)$filter_cuisine)?true:false
						  ,array(
						  'value'=>$val['cuisine_id'],
						  'class'=>"filter_by icheck filter_cuisine"
						  ));
						  ?>
						  <?php echo $val['cuisine_name']?>
						 </label> </li>
						 <?php endforeach;?> 
					   </ul>
				   </div> <!--filter-box-->
				   <?php endif;?>
				   <!--END FILTER CUISINE-->
					
				
					<!--MINIUM DELIVERY FEE-->           
           <?php if ( $minimum_list=FunctionsV3::minimumDeliveryFee()):?>
           <div class="filter_type">
	           <h6> 
	             <?php echo t("Minimum Delivery")?>
	             </h6>
	            <ul class="<?php echo $fc==2?"hide":''?>">
	             <?php foreach ($minimum_list as $key=>$val):?>
	              <li>
		           <?php 
		          echo CHtml::radioButton('filter_minimum[]',
		          $filter_minimum==$key?true:false
		          ,array(
		          'value'=>$key,
		          'class'=>"filter_by_radio filter_minimum icheck"
		          ));
		          ?>
	              <?php echo $val;?>
	              </li>
	             <?php endforeach;?> 
	           </ul>
           </div> <!--filter-box-->
           <?php endif;?>
           <!--END MINIUM DELIVERY FEE-->
					
					
					
				</div><!--End collapse -->
			</div><!--End filters col-->
		</div><!--End col-md -->
   <style>
.result-merchant ul.services-type {
  margin:0;
}
.section-feature-resto ul li, ul.services-type li {
	padding: 0;
}
.grid .services-type {
  margin: 0 auto !important;
}
</style>     
		<div class="col-md-9">
        <?php echo CHtml::hiddenField('sort_filter',$sort_filter)?>
     <?php echo CHtml::hiddenField('display_type',$display_type)?>    
        <div id="tools">
				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-6">
						<div class="styled-select">
								<?php 
								   $filter_list=array(
									 'restaurant_name'=>t("Name"),
									 'ratings'=>t("Rating"),
									 'minimum_order'=>t("Minimum"),
									 'distance'=>t("Distance")
								   );
								   if (isset($_GET['st'])){
									   unset($filter_list['distance']);
								   }
								   echo CHtml::dropDownList('sort-results',$sort_filter,$filter_list,array(
									 'class'=>"sort-results",
									 'title'=>t("Sort By")
								   ));
								   ?> 
						</div>
					</div>
					<div class="col-md-9 col-sm-6 hidden-xs">
						
						
						<a href="<?php echo FunctionsV3::clearSearchParams('','display_type=listview')?>" 
						   class="bt_filters <?php echo $display_type=="gridview"?'inactive':''?>" 
							  data-type="listview">
							<i class="icon-list"></i>
						</a>
							
						<a href="<?php echo FunctionsV3::clearSearchParams('','display_type=gridview')?>" 
							  class="bt_filters <?php echo $display_type=="listview"?'inactive':''?>" 
							  data-type="gridview">
							<i class="icon-th"></i>
						</a> 
						
						
						
					</div>
				</div>
			</div><!--End tools -->
        
			<!--MERCHANT LIST -->
                  
         <div class="result-merchant">
             <div class="row infinite-container">
             
             <?php if ($data):?>
	             <?php foreach ($data['list'] as $val):?>
	             <?php 
	             $merchant_id=$val['merchant_id'];             
	             $ratings=Yii::app()->functions->getRatings($merchant_id);   
	             
	             /*get the distance from client address to merchant Address*/             
	             $distance_type=FunctionsV3::getMerchantDistanceType($merchant_id); 
	             $distance_type_orig=$distance_type;
	             
	             /*dump("c lat=>".$data['client']['lat']);         
	             dump("c lng=>".$data['client']['long']);	             
	             dump("m lat=>".$val['latitude']);
	             dump("c lng=>".$val['lontitude']);*/
	             
	               
	             $distance=FunctionsV3::getDistanceBetweenPlot(
	                $data['client']['lat'],$data['client']['long'],
	                $val['latitude'],$val['lontitude'],$distance_type
	             );      
	             	             	     
	             $distance_type_raw = $distance_type=="M"?"miles":"kilometers";
	             $distance_type = $distance_type=="M"?t("miles"):t("kilometers");
	             $distance_type_orig = $distance_type_orig=="M"?t("miles"):t("kilometers");
	             
	             if(!empty(FunctionsV3::$distance_type_result)){
	             	$distance_type_raw=FunctionsV3::$distance_type_result;
	             	$distance_type=t(FunctionsV3::$distance_type_result);
	             }
	             
	             $merchant_delivery_distance=getOption($merchant_id,'merchant_delivery_miles');             
	             
	             $delivery_fee=FunctionsV3::getMerchantDeliveryFee(
	                          $merchant_id,
	                          $val['delivery_charges'],
	                          $distance,
	                          $distance_type_raw);
	             ?>
	             
	             <?php 	             
	             if ( $display_type=="listview"){
	             	 $this->renderPartial('/front/search-list-2',array(
					   'data'=>$data,
					   'val'=>$val,
					   'merchant_id'=>$merchant_id,
					   'ratings'=>$ratings,
					   'distance_type'=>$distance_type,
					   'distance_type_orig'=>$distance_type_orig,
					   'distance'=>$distance,
					   'merchant_delivery_distance'=>$merchant_delivery_distance,
					   'delivery_fee'=>$delivery_fee
					 ));
	             } else {
		             $this->renderPartial('/front/search-list-1',array(
					   'data'=>$data,
					   'val'=>$val,
					   'merchant_id'=>$merchant_id,
					   'ratings'=>$ratings,
					   'distance_type'=>$distance_type,
					   'distance_type_orig'=>$distance_type_orig,
					   'distance'=>$distance,
					   'merchant_delivery_distance'=>$merchant_delivery_distance,
					   'delivery_fee'=>$delivery_fee
					 ));
	             }
				 ?>
				              
	              <?php endforeach;?>     
              <?php else :?>     
              <p class="center top25 text-danger"><?php echo t("No results with your selected filters")?></p>
              <?php endif;?>
                                                   
             </div> <!--row-->                
             <a href="#0" style="max-width:100%"  class="load_more_bt wow fadeIn search-result-loader" data-wow-delay="0.2s">Load more...</a>
         
             
             <?php                         
             if (!isset($current_page_url)){
             	$current_page_url='';
             }
             if (!isset($current_page_link)){
             	$current_page_link='';
             }
             echo CHtml::hiddenField('current_page_url',$current_page_url);
             require_once('pagination.class.php'); 
             $attributes                 =   array();
			 $attributes['wrapper']      =   array('id'=>'pagination','class'=>'pagination');			 
			 $options                    =   array();
			 $options['attributes']      =   $attributes;
			 $options['items_per_page']  =   FunctionsV3::getPerPage();
			 $options['maxpages']        =   1;
			 $options['jumpers']=false;
			 $options['link_url']=$current_page_link.'&page=##ID##';			
			 $pagination =   new pagination( $data['total'] ,((isset($_GET['page'])) ? $_GET['page']:1),$options);		
			 $data   =   $pagination->render();
             ?>             
                    
         </div> <!--result-merchant-->
              
		</div><!-- End col-md-9-->
        
	</div><!-- End row -->
</div><!-- End container -->
<!-- End Content =============================================== -->







