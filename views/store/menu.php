<?php
/*POINTS PROGRAM*/
if (Yii::app()->hasModule('pointsprogram')){
	unset($_SESSION['pts_redeem_amt']);
	unset($_SESSION['pts_redeem_points']);
}

$merchant_photo_bg=getOption($merchant_id,'merchant_photo_bg');
if ( !file_exists(FunctionsV3::uploadPath()."/$merchant_photo_bg")){
	$merchant_photo_bg='';
} 

/*RENDER MENU HEADER FILE*/
$ratings=Yii::app()->functions->getRatings($merchant_id);   
$merchant_info=array(   
  'merchant_id'=>$merchant_id ,
  'minimum_order'=>$data['minimum_order'],
  'ratings'=>$ratings,
  'merchant_address'=>$data['merchant_address'],
  'cuisine'=>$data['cuisine'],
  'restaurant_name'=>$data['restaurant_name'],
  'background'=>$merchant_photo_bg,
  'merchant_website'=>$merchant_website,
  'merchant_logo'=>FunctionsV3::getMerchantLogo($merchant_id)
);
$this->renderPartial('/front/menu-header',$merchant_info);

/*ADD MERCHANT INFO AS JSON */
$cs = Yii::app()->getClientScript();
$cs->registerScript(
  'merchant_information',
  "var merchant_information =".json_encode($merchant_info)."",
  CClientScript::POS_HEAD
);		

/*PROGRESS ORDER BAR*/
$this->renderPartial('/front/order-progress-bar',array(
   'step'=>3,
   'show_bar'=>true
));

$now=date('Y-m-d');
$now_time='';

$checkout=FunctionsV3::isMerchantcanCheckout($merchant_id); 
$menu=Yii::app()->functions->getMerchantMenu($merchant_id); 

echo CHtml::hiddenField('is_merchant_open',isset($checkout['code'])?$checkout['code']:'' );

/*hidden TEXT*/
echo CHtml::hiddenField('restaurant_slug',$data['restaurant_slug']);
echo CHtml::hiddenField('merchant_id',$merchant_id);
echo CHtml::hiddenField('is_client_login',Yii::app()->functions->isClientLogin());

echo CHtml::hiddenField('website_disbaled_auto_cart',
Yii::app()->functions->getOptionAdmin('website_disbaled_auto_cart'));

$hide_foodprice=Yii::app()->functions->getOptionAdmin('website_hide_foodprice');
echo CHtml::hiddenField('hide_foodprice',$hide_foodprice);

echo CHtml::hiddenField('accept_booking_sameday',getOption($merchant_id
,'accept_booking_sameday'));

echo CHtml::hiddenField('customer_ask_address',getOptionA('customer_ask_address'));

echo CHtml::hiddenField('merchant_required_delivery_time',
  Yii::app()->functions->getOption("merchant_required_delivery_time",$merchant_id));   
  
/** add minimum order for pickup status*/
$merchant_minimum_order_pickup=Yii::app()->functions->getOption('merchant_minimum_order_pickup',$merchant_id);
if (!empty($merchant_minimum_order_pickup)){
	  echo CHtml::hiddenField('merchant_minimum_order_pickup',$merchant_minimum_order_pickup);
	  
	  echo CHtml::hiddenField('merchant_minimum_order_pickup_pretty',
         displayPrice(baseCurrency(),prettyFormat($merchant_minimum_order_pickup)));
}
 
$merchant_maximum_order_pickup=Yii::app()->functions->getOption('merchant_maximum_order_pickup',$merchant_id);
if (!empty($merchant_maximum_order_pickup)){
	  echo CHtml::hiddenField('merchant_maximum_order_pickup',$merchant_maximum_order_pickup);
	  
	  echo CHtml::hiddenField('merchant_maximum_order_pickup_pretty',
         displayPrice(baseCurrency(),prettyFormat($merchant_maximum_order_pickup)));
}  

/*add minimum and max for delivery*/
$minimum_order=Yii::app()->functions->getOption('merchant_minimum_order',$merchant_id);
if (!empty($minimum_order)){
	echo CHtml::hiddenField('minimum_order',unPrettyPrice($minimum_order));
	echo CHtml::hiddenField('minimum_order_pretty',
	 displayPrice(baseCurrency(),prettyFormat($minimum_order))
	);
}
$merchant_maximum_order=Yii::app()->functions->getOption("merchant_maximum_order",$merchant_id);
 if (is_numeric($merchant_maximum_order)){
 	echo CHtml::hiddenField('merchant_maximum_order',unPrettyPrice($merchant_maximum_order));
    echo CHtml::hiddenField('merchant_maximum_order_pretty',baseCurrency().prettyFormat($merchant_maximum_order));
 }

$is_ok_delivered=1;
if (is_numeric($merchant_delivery_distance)){
	if ( $distance>$merchant_delivery_distance){
		$is_ok_delivered=2;
		/*check if distance type is feet and meters*/
		if($distance_type=="ft" || $distance_type=="mm" || $distance_type=="mt"){
			$is_ok_delivered=1;
		}
	}
} 

echo CHtml::hiddenField('is_ok_delivered',$is_ok_delivered);
echo CHtml::hiddenField('merchant_delivery_miles',$merchant_delivery_distance);
echo CHtml::hiddenField('unit_distance',$distance_type);
echo CHtml::hiddenField('from_address', FunctionsV3::getSessionAddress() );

echo CHtml::hiddenField('merchant_close_store',getOption($merchant_id,'merchant_close_store'));
/*$close_msg=getOption($merchant_id,'merchant_close_msg');
if(empty($close_msg)){
	$close_msg=t("This restaurant is closed now. Please check the opening times.");
}*/
echo CHtml::hiddenField('merchant_close_msg',
isset($checkout['msg'])?$checkout['msg']:t("Sorry merchant is closed."));

echo CHtml::hiddenField('disabled_website_ordering',getOptionA('disabled_website_ordering'));
echo CHtml::hiddenField('web_session_id',session_id());

echo CHtml::hiddenField('merchant_map_latitude',$data['latitude']);
echo CHtml::hiddenField('merchant_map_longtitude',$data['lontitude']);
echo CHtml::hiddenField('restaurant_name',$data['restaurant_name']);
?>

<div class="sections section-menu section-grey2">
<div class="container">
  <div class="row">

	<div class="col-md-12 border menu-left-content">
         
        <div class="tabs-wrapper" id="menu-tab-wrapper">
	    <ul id="tabs" style="margin-bottom:0;border-bottom:none">
		    <li class="active">
		       <span><?php echo t("Menu")?></span>
		       <i class="ion-fork"></i>
		    </li>
		     <?php if ($booking_enabled):?>
		      <li>
		      <span><?php echo t("Reservation")?></span>
		      <i class="ion-coffee"></i>
		      </li>
		    <?php endif;?>
			
			 <?php if ($theme_reviews_tab==""):?>
		    <li class="view-reviews">
		       <span><?php echo t("Reviews")?></span>
		       <i class="ion-ios-star-half"></i>
		    </li>
		    <?php endif;?>
			 <?php if ( $promo['enabled']==2 && $theme_promo_tab==""):?>
		      <li>
		       <span><?php echo t("Promotions")?></span>
		       <i class="ion-ribbon-b"></i>
		      </li>
		    <?php endif;?>
			
		  
		     <?php if ($photo_enabled):?>
		      <li class="view-merchant-photos">
		       <span><?php echo t("Instagram")?></span>
		       <i class="ion-images"></i>
		      </li>
		    <?php endif;?>
		    
		    <?php if ($theme_map_tab==""):?>
		    <li class="view-merchant-map">
		       <span><?php echo t("Get Directions")?></span>
		       <i class="ion-ios-navigate-outline"></i>
		    </li>
		    <?php endif;?>
		   
		    
		    <?php if ($theme_info_tab==""):?>
		    <li>
		      <span><?php echo t("About us")?></span>
		      <i class="ion-ios-information-outline"></i>
		    </li>
		    <?php endif;?>
		    
		     <?php/*  if ($theme_hours_tab==""):?>
		    <li>
		       <span><?php echo t("Opening Hours")?></span>
		       <i class="ion-clock"></i>
		    </li>
		    <?php endif; */?>
		    
		</ul>
		</div>
	</div>
		
		
		
		
		 
		
	<div class="col-md-9">	
		
		<ul id="tab">
		
		<!--MENU-->
	    <li class="active">
	        <div class="row">
			 <div class="col-xs-12 col-sm-6 col-md-4 border sspading">
				<p><a href="http://zendeliver.net/store/" class="btn_side">Back to search</a></p>
				<div class="box_style_1">
				 <?php 
				 $this->renderPartial('/front/menu-category',array(
				  'merchant_id'=>$merchant_id,
				  'menu'=>$menu			  
				 ));
				 ?>
				</div>
				<div class="box_style_2 hidden-xs" id="help">
					<i class="ion-ribbon-b"></i>
					<h4>Need <span>Help?</span></h4>
					<a href="tel://004542344599" class="phone">888-893-2930</a>
					<small>Monday to Friday 9.00am - 7.30pm</small>
				</div>
			 </div> <!--col-->
			 <div class="col-md-8 col-xs-8 border" id="menu-list-wrapper" style="padding: 0px 10px 0px 18px;">
			 <?php 
			 switch (getOptionA('admin_activated_menu'))
			 {
			 	case 1:
			 		$this->renderPartial('/front/menu-merchant-2',array(
					  'merchant_id'=>$merchant_id,
					  'menu'=>$menu,
					  'disabled_addcart'=>$disabled_addcart
					));
			 		break;
			 		
			 	case 2:
			 		$this->renderPartial('/front/menu-merchant-3',array(
					  'merchant_id'=>$merchant_id,
					  'menu'=>$menu,
					  'disabled_addcart'=>$disabled_addcart
					));
			 		break;
			 			
			 	default:	
				 	$this->renderPartial('/front/menu-merchant-1',array(
					  'merchant_id'=>$merchant_id,
					  'menu'=>$menu,
					  'disabled_addcart'=>$disabled_addcart,
					  'tc'=>$tc
					));
			    break;
			 }			 
			 ?>			
			 </div> <!--col-->
			</div> <!--row-->
	    </li>
	    <!--END MENU-->
	    
		
		<!--BOOK A TABLE-->
	    <?php if ($booking_enabled):?>
	    <li>
	    <?php $this->renderPartial('/front/merchant-book-table',array(
	      'merchant_id'=>$merchant_id
	    )); ?>        
	    </li>
	    <?php endif;?>
	    <!--END BOOK A TABLE-->
	    
		
		  <!--MERCHANT REVIEW-->
	    <?php if ($theme_reviews_tab==""):?>
	    <li class="review-tab-content">	       	     
	    <?php $this->renderPartial('/front/merchant-review',array(
	      'merchant_id'=>$merchant_id
	    )); ?>           
	    </li>
	    <?php endif;?>
	    <!--END MERCHANT REVIEW-->
		
		
		 <!--PROMOS-->
	    <?php if ( $promo['enabled']==2 && $theme_promo_tab==""):?>
	    <li>
	    <?php $this->renderPartial('/front/merchant-promo',array(
	      'merchant_id'=>$merchant_id,
	      'promo'=>$promo
	    )); ?>        
	    </li>
	    <?php endif;?>
	    <!--END PROMOS-->
		
		
		    
	    <!--PHOTOS-->
	    <?php if ($photo_enabled):?>
	    <li>
	    <?php 
	    $gallery=Yii::app()->functions->getOption("merchant_gallery",$merchant_id);
        $gallery=!empty($gallery)?json_decode($gallery):false;
	    $this->renderPartial('/front/merchant-photos',array(
	      'merchant_id'=>$merchant_id,
	      'gallery'=>$gallery
	    )); ?>        
	    </li>
	    <?php endif;?>
	    <!--END PHOTOS-->
		
		
			  
	    
	    <!--MERCHANT MAP-->
	    <?php if ($theme_map_tab==""):?>
	    <li>	        	
	    <?php $this->renderPartial('/front/merchant-map'); ?>        
	    </li>
	    <?php endif;?>
	    <!--END MERCHANT MAP-->
		
		
		
	    
	    <!--INFORMATION-->
	    <?php if ($theme_info_tab==""):?>
	    <li>
	        <div class="box-grey rounded " style="margin-top:0;">
	          <?php echo getOption($merchant_id,'merchant_information')?>
	        </div>
	    </li>
	    <?php endif;?>
	    <!--END INFORMATION-->
		
		
	    <!--OPENING HOURS-->
	    <?php if ($theme_hours_tab==""):?>
	    <li>	       	     
	    <?php
	    $this->renderPartial('/front/merchant-hours',array(
	      'merchant_id'=>$merchant_id
	    )); ?>           
	    </li>
	    <?php endif;?>
	    <!--END OPENING HOURS-->
	    

	    
	    
	
	    
	   
	    
	    
	   </ul>
	   </div>
     
	 
	 
	 
	 
	 
	 
	 
	 
	 
	
     
     <?php if (getOptionA('disabled_website_ordering')!="yes"):?>
	 
	 
	 
	 <div class="col-md-3" id="sidebar" style=" padding-left: 0;margin-top:10px">
            <div class="theiaStickySidebar">
				<div id="cart_box" >
					<h3><?php echo t("Merchant Information")?><i class="ion-navigate pull-right"></i></h3>
					<table class="table table_summary">
					<tbody>
					<tr>
						<td><?php 
							if ($distance){
								echo t("Distance From Me").": ".number_format($distance,1)." $distance_type";
							} else echo  t("Distance").": ".t("not available");
							?>
						</td>
					</tr>			
					<tr class="delivery-fee-wrap">
						<td><?php echo t("Estimated Wait")?>: <?php echo FunctionsV3::getDeliveryEstimation($merchant_id)?></td>
					</tr>
					<tr class="delivery-fee-wrap">
						<td><?php 
							if (!empty($merchant_delivery_distance)){
								echo t("Delivery Distance Covered").": ".$merchant_delivery_distance." $distance_type_orig";
							} else echo  t("Area Covered").": ".t("Not Available");
							?></td>
					<tr>
						<td><a href="javascript:;" class="top10 green-color change-address block text-center">
						[<?php echo t("Change Your Address Here")?>]
						</a></td>
					</tr>
					
					</tbody>
					</table>
					
				
				
					<h3><?php echo t("Your Basket")?> <i class="icon_cart_alt pull-right"></i></h3>
					<div class="item">
					<table class="table table_summary">
					<tbody class="item-order-wrap">
					
					</tbody>
					</table>
					<hr>
					<div class="row" id="options_2">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							 <!--DELIVERY OPTIONS-->
								<div class="inner line-top relative delivery-option center">
								  
								   <h3 style="text-align:left"><?php echo t("Fulfillment Options")?><i class="icon-stopwatch-1 pull-right"></i></h3>
								  
								   <?php  echo CHtml::dropDownList('delivery_type',$now,
								   (array)Yii::app()->functions->DeliveryOptions($merchant_id),array(
									 'class'=>'grey-fields'
								   )) ?>
								   
								   <?php echo CHtml::hiddenField('delivery_date',$now)?>
								   <?php echo CHtml::textField('delivery_date1',
									FormatDateTime($now,false),array('class'=>"j_date grey-fields",'data-id'=>'delivery_date'))?>
								   
								   <div class="delivery_asap_wrap"> 
									 <?php echo CHtml::textField('delivery_me',$now_time,
									  array('class'=>"timepick grey-fields",'placeholder'=>Yii::t("default","Time")))?>	       
									  <span class="delivery-asap">
									  <?php echo CHtml::checkBox('delivery_asap',false,array('class'=>"icheck"))?>
									  <span class="text-muted"><?php echo Yii::t("default","Need ASAP?")?></span>	          
									 </span>       	         	        
								   </div><!-- delivery_asap_wrap-->
								   
								   <?php if ( $checkout['code']==1):?>
									  <a href="javascript:;" class="btn_full checkout"><?php echo $checkout['button']?></a>
								   <?php else :?>
									  <?php if ( $checkout['holiday']==1):?>
										 <?php echo CHtml::hiddenField('is_holiday',$checkout['msg'],array('class'=>'is_holiday'));?>
										 <p class="text-danger"><?php echo $checkout['msg']?></p>
									  <?php else :?>
										 <p class="text-danger"><?php echo $checkout['msg']?></p>
										 <p class="small">
										 <?php echo Yii::app()->functions->translateDate(date('F d l')."@".timeFormat(date('c'),true));?></p>
									  <?php endif;?>
								   <?php endif;?>
																						
								</div> <!--inner-->
								<!--END DELIVERY OPTIONS-->
						</div></div>
						
					</div><!-- Edn options 2 -->
                
				</div><!-- End cart_box -->
                </div><!-- End theiaStickySidebar -->
			</div><!-- End col-md-3 -->
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
     
     <?php endif;?>
  </div> <!-- menu-left-content-->

</div> <!--container-->
</div> <!--section-menu-->